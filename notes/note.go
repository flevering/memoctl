/*
Package notes is used for everything around creating, storing, updating and deleting notes
*/
package notes

import (
	"fmt"
	"os"

	"github.com/google/uuid"
)

/*
Note is a struct containing all properties of a note
*/
type Note struct {
	ID          uuid.UUID
	Title       string
	Description string
	Finished    bool
}

var file *os.File

func check(e error) {
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
}

/*
GetTitle will return the current Note title
*/
func (n Note) GetTitle() string {
	return n.Title
}

/*
GetDescription will return the current Note description
*/
func (n Note) GetDescription() string {
	return n.Description
}

/*
GetID will return the current Note ID
*/
func (n Note) GetID() uuid.UUID {
	return n.ID
}

/*
MarkAsDone will set Finished to true
*/
func (n *Note) MarkAsDone() {
	n.Finished = true
}

/*
NewNote will create a new Note of type struct
*/
func NewNote(title string, description string) *Note {
	note := Note{ID: uuid.New(), Title: title, Description: description, Finished: false}
	return &note
}
