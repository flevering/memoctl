/*
Package notes is used for everything around creating, storing, updating and deleting notes
*/
package notes

import (
	"encoding/json"
	"log"
	"os"
	"strings"

	"github.com/franklevering/memoctl/helpers/config"
	bolt "go.etcd.io/bbolt"
)

/*
GetUnfinishedNotes will get all the notes from storage that are not finished
*/
func GetUnfinishedNotes() []Note {
	db := openDb()
	defer closeDb(db)

	var note Note
	var notes []Note

	db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Notes"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			json.Unmarshal(v, &note)
			if !note.Finished {
				notes = append(notes, note)
			}
		}

		return nil
	})

	return notes
}

/*
GetNotesByValue will loop through all notes and find notes that are matching the title
*/
func GetNotesByValue(noteSubstr string) []Note {
	db := openDb()
	defer closeDb(db)

	var note Note
	var notes []Note

	db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		b := tx.Bucket([]byte("Notes"))

		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			json.Unmarshal(v, &note)
			if strings.Contains(strings.ToUpper(note.Title), strings.ToUpper(noteSubstr)) && !note.Finished {
				notes = append(notes, note)
			}
		}

		return nil
	})

	return notes
}

/*
MarkNotesAsDone will set the Finished property to true
*/
func MarkNotesAsDone(notes []Note) {
	db := openDb()
	defer closeDb(db)

	for i := range notes {
		notes[i].MarkAsDone()
	}

	dbErr := db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte("Notes"))
		check(err)
		for _, note := range notes {
			noteString, noteErr := json.Marshal(note)
			check(noteErr)

			bucket.Put([]byte(note.ID.String()), noteString)
		}
		return nil
	})

	check(dbErr)
}

/*
WriteNote will create a new note in storage
*/
func WriteNote(note *Note) error {
	db := openDb()
	defer closeDb(db)

	noteString, noteErr := json.Marshal(note)

	check(noteErr)

	dbErr := db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte("Notes"))
		check(err)
		bucket.Put([]byte(note.ID.String()), noteString)
		return nil
	})

	check(dbErr)

	return nil
}

/*
DeleteNotes will delete a note from storage
*/
func DeleteNotes(notes []string) error {
	db := openDb()
	defer closeDb(db)

	var ids []string

	for _, note := range notes {
		id := note[strings.Index(note, "-")+1:]
		id = strings.Replace(id, " ", "", -1)
		ids = append(ids, id)
	}

	for _, id := range ids {
		db.Update(func(tx *bolt.Tx) error {
			bucket, err := tx.CreateBucketIfNotExists([]byte("Notes"))
			check(err)
			bucket.Delete([]byte(id))
			return nil
		})
	}

	return nil
}

func openDb() *bolt.DB {
	path := config.GetPath()
	db, err := bolt.Open(path+"notes.db", 0600, nil)

	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	return db
}

func closeDb(db *bolt.DB) {
	db.Close()
}
