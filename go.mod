module github.com/franklevering/memoctl

go 1.13

require (
	github.com/AlecAivazis/survey/v2 v2.0.4
	github.com/fatih/color v1.7.0
	github.com/google/uuid v1.1.1
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	go.etcd.io/bbolt v1.3.3
)
