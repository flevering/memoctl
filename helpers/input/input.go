/*
Package input is used for asking for input from users
*/
package input

import (
	"github.com/AlecAivazis/survey/v2"
)

/*
Input struct containing methods around asking for input
*/
type Input struct{}

/*
AskQuestion is to ask a question to the user
*/
func (*Input) AskQuestion(message string, required bool) string {
	input := &survey.Input{
		Message: message,
	}

	return ask(input, required)
}

/*
AskMultiline is to ask a multi line question to the user
*/
func (*Input) AskMultiline(message string, required bool) string {
	input := &survey.Multiline{
		Message: message,
	}

	return ask(input, required)
}

/*
SelectFromList will populate a list of options to select from
Method expects a question and a list of selectable options
*/
func (*Input) SelectFromList(question string, options []string) []string {
	input := &survey.MultiSelect{
		Message: question,
		Options: options,
	}

	return askMulti(input)
}

func ask(input survey.Prompt, required bool) string {
	var answer string

	if required {
		survey.AskOne(input, &answer, survey.WithValidator(survey.Required))
	} else {
		survey.AskOne(input, &answer)
	}

	return answer
}

func askMulti(input survey.Prompt) []string {
	var answers []string

	survey.AskOne(input, &answers)

	return answers
}
