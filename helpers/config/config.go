/*
Package config contains all methods and structs in order to work with config
*/
package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/viper"
)

/*
GetPath will get the path of the current configuration file
*/
func GetPath() string {
	path := viper.GetString("storage.directory")

	if !strings.HasSuffix(path, "/") {
		path = path + "/"
	}

	if _, err := os.Stat(path); os.IsNotExist(err) {
		fmt.Println("Directory does not exist")
	}

	return path
}
