/*
Package cmd is the package containing all cli commands
*/
package cmd

import (
	"github.com/franklevering/memoctl/helpers/input"
	"github.com/franklevering/memoctl/notes"
	"github.com/spf13/cobra"
)

// notesmd represents the notes command
var notesDeleteCmd = &cobra.Command{
	Use:     "notes",
	Aliases: []string{"note"},
	Short:   "Deleting notes from your list",
	Run: func(cmd *cobra.Command, args []string) {
		deleteNote(cmd, args)
	},
}

func deleteNote(cmd *cobra.Command, args []string) {
	allNotes := notes.GetUnfinishedNotes()
	notesList := []string{}

	for _, note := range allNotes {
		notesList = append(notesList, note.GetTitle()+" - "+note.GetID().String())
	}

	input := input.Input{}

	notesToBeDeleted := input.SelectFromList("Which notes would you like to delete?", notesList)

	notes.DeleteNotes(notesToBeDeleted)

}

func init() {
	deleteCmd.AddCommand(notesDeleteCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// notesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// notesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
