/*
Package cmd is the package containing all cli commands
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/franklevering/memoctl/notes"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

// notesCmd represents the notes command
var notesCmd = &cobra.Command{
	Use:     "notes",
	Aliases: []string{"note"},
	Short:   "Get an overview of all your notes",
	Run: func(cmd *cobra.Command, args []string) {
		getNotes(cmd, args)
	},
}

func getNotes(cmd *cobra.Command, args []string) {
	notes := notes.GetUnfinishedNotes()

	fmt.Println(notes)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetRowLine(true)
	table.SetHeader([]string{"ID", "Note Title", "Note Description"})
	table.SetRowSeparator("-")

	for _, note := range notes {
		table.Append([]string{note.GetID().String(), string(note.GetTitle()), note.GetDescription()})
	}

	table.Render()
}

func init() {
	listCmd.AddCommand(notesCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// notesCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// notesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
