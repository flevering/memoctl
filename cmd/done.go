/*
Package cmd is the package containing all cli commands
*/
package cmd

import (
	"fmt"
	"strings"

	"github.com/franklevering/memoctl/helpers/input"

	"github.com/fatih/color"
	"github.com/franklevering/memoctl/notes"
	"github.com/spf13/cobra"
)

// doneCmd represents the done command
var doneCmd = &cobra.Command{
	Use:   "done",
	Short: "Mark a note as done",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return fmt.Errorf("requires a Task argument")
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		markAsDone(cmd, args)
	},
}

func markAsDone(cmd *cobra.Command, args []string) {
	note := args[0]

	notesByValue := notes.GetNotesByValue(note)

	if len(notesByValue) <= 0 {
		color.Red("No notes found")
	}

	var titles []string

	for _, note := range notesByValue {
		titles = append(titles, note.Title)
	}

	input := input.Input{}

	selectedNotes := input.SelectFromList("Select tasks to be done", titles)

	if len(selectedNotes) <= 0 {
		color.Cyan("No notes selected")
	}

	var finishedNotes []notes.Note

	for _, title := range selectedNotes {
		for _, note := range notesByValue {
			if strings.EqualFold(title, note.Title) {
				finishedNotes = append(finishedNotes, note)
			}
		}
	}

	notes.MarkNotesAsDone(finishedNotes)
}

func init() {
	rootCmd.AddCommand(doneCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// doneCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// doneCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
